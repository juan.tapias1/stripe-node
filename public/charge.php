<?php
require('/home/moodledemo/public_html/config.php');
$sk_token = get_config("local_nsq_core", "secretkey");
require_once('../vendor/autoload.php');
\Stripe\Stripe::setApiKey($sk_token);
// Get the token from the JS script
$token          = $_POST['stripeToken'];
$customerEmail  = $_POST['stripeCustomerEmail'];
$planPrice      = $_POST['stripePricePlan'];

// Create a Customer
$customer = \Stripe\Customer::create(array(
    "email" => $customerEmail,
    "source" => $token,
));
// Save the customer id in your own database!
// Charge the Customer instead of the card
$charge = \Stripe\Charge::create(array(
    "amount" => $planPrice,
    "currency" => "usd",
    "customer" => $customer->id
));
// You can charge the customer later by using the customer id.
// Redirect to app
$urlApp = "https://moodledemo.soluttolabs.com/signup-final/";
header('Location:'.$urlApp);