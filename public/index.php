<?php
require('/home/moodledemo/public_html/config.php');
$pk_token = get_config("local_nsq_core", "publishkey");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/style.css"/>
</head>
<body>
    <section class="checkout-stripe">
        <div class="container ">
            <div class="row h-100">
                <div class="col-12 col-md-6 m-auto">
                    <figure class="logo">
                        <img src="./img/evergreen-learners.png" alt="Evergreeen Learners" class="img-fluid" width="auto" height="auto">
                    </figure>
                    <form action="./charge.php" method="post" id="payment-form">
                        <input type="hidden" name="stripeToken" id="stripe-token" value="<?php echo $pk_token; ?>">
                        <div class="form-row form-group">
                            <label for="card-element">Customer Email</label>
                            <input type="email" id="customer-email" name="stripeCustomerEmail" class="form-control" disabled>
                        </div>
                        <div class="form-row form-group">
                            <label for="card-element">Credit or debit card</label>
                            <div id="card-element">
                            <!-- a Stripe Element will be inserted here. -->
                            </div>
                            <!-- Used to display form errors -->
                            <div id="card-errors"></div>
                        </div>
                        <div class="form-row">
                            <div class="col-12 col-md-4 m-auto">
                                <button class="btn btn-payment">Submit Payment</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- .col -->
            </div>
            <!-- .row -->
        </div>
        <!-- .container -->
    </section>
    <!-- .checkout-stripe -->

<!-- The needed JS files -->
<!-- JQUERY File -->
<script src="./js/jquery.min.js"></script>
<!-- Stripe JS -->
<script src="https://js.stripe.com/v3/"></script>
<!-- Your JS File -->
<script src="./charge.js"></script>
</body>
</html>